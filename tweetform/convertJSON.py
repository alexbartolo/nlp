import json

lines = []

with open("train_us_semeval18.ids", "r") as f:
    lines = f.read().splitlines() 

toSave = {"current_ix": 0, "tweet_ids": lines}

with open("train_us_semeval18.json", "w") as write_file:
    json.dump(toSave, write_file)
